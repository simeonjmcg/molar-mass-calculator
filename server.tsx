import * as path from "path";
import * as express from "express";

import * as React from "react";
import { renderToString } from "react-dom/server";
import { Provider } from "react-redux";
import { StaticRouter, Route } from "react-router-dom";
import { SheetsRegistry } from 'jss';
import JssProvider from 'react-jss/lib/JssProvider';
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from "@material-ui/core/styles";

import { theme } from "./src/theme";

import configureStore from "./src/store/configureStore";
import { App } from "./src/components/App";
import { AppData } from "./src/types";

const port: any = process.env.PORT || 8081;
const ip: string = process.env.IP || "127.0.0.1";
const env: string = process.env.NODE_ENV || "development";

// initialize the server and configure support for ejs templates
const app = express();
app.set("view engine", "ejs");
app.set("views", __dirname);

if(env == "production") {
  // define the folder that will be used for static assets
  app.use(express.static(path.join(__dirname, "dist", "static")));
} else {
  //load config
  const webpackConfig = require ("./webpack.dev");

  // Use this middleware to set up hot module reloading via webpack.
  const compiler = require("webpack")(webpackConfig);
  app.use(require("webpack-dev-middleware")(compiler, { noInfo: true, publicPath: webpackConfig.output.publicPath }));
  app.use(require("webpack-hot-middleware")(compiler, {
    log: console.log,
    path: "/__webpack_hmr",
    heartbeat: 10 * 1000,
  }));
  app.use(express.static(path.join(__dirname, "dist", "static")));
}

// universal routing and rendering
const handleRender = (req: any, res: any) => {
  // Create a new Redux store instance
  const store = configureStore({});
  // Render the component to a string
  
  // Create a sheetsRegistry instance.
  const sheetsRegistry = new SheetsRegistry();

  // Create a sheetsManager instance.
  const sheetsManager = new Map();

  // Create a new class name generator.
  const generateClassName = createGenerateClassName();
  const html = renderToString(
    <JssProvider registry={sheetsRegistry} generateClassName={generateClassName}>
      <MuiThemeProvider theme={theme} sheetsManager={sheetsManager}>
        <Provider store={store}>
          <StaticRouter location={req.url} context={{}}>
            <App />
          </StaticRouter>
        </Provider> 
      </MuiThemeProvider>
    </JssProvider>
  );
  // Grab the initial state from our Redux store
  const finalState = store.getState(); 

  // Grab the CSS from our sheetsRegistry.
  const css = sheetsRegistry.toString()

  // Send the rendered page back to the client, using "index.ejs" template.
  res.render("index", { html, css, preload: JSON.stringify(finalState).replace(/</g, "\\x3c")});// preload sanitizes so <script> is not accidentally breached in template
};

app.use(handleRender);

// start the server
app.listen(port, ip, (err: any) => {
  if (err) {
    return console.error(err);
  }
  console.info(`Server running on http://${ip}:${port} [${env}]`);
});
