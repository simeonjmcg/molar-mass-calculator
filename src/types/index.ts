export interface AppData {
  sideNavOpened?: boolean;
  input: string;
  output: string;
}
