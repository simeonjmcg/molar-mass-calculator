import { Reducer, Action } from "redux";
import { SET_INPUT, SET_OUTPUT, OPEN_SIDE_NAV, CLOSE_SIDE_NAV } from "../actions";
import { AppData } from "../types";
import { isType } from "typescript-fsa";


const rootReducer: Reducer<AppData> = (state: AppData = { input: "", output: "" }, action: Action) => {
  if (isType(action, SET_INPUT))
    return { ...state, input: action.payload as string };
  if (isType(action, SET_OUTPUT))
    return { ...state, output: action.payload as string };
  if (isType(action, OPEN_SIDE_NAV)) {
    return { ...state, sideNavOpened: true };
  }
  if (isType(action, CLOSE_SIDE_NAV)) {
    return { ...state, sideNavOpened: false };
  }
  return state;
};

export default rootReducer;
