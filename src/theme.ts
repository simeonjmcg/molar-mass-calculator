import { createMuiTheme } from "@material-ui/core/styles";
import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";

export const theme = createMuiTheme({ 
  typography: {
    useNextVariants: true,
  }, palette: {
    primary: green,
    secondary: red,
    type: "light",
  },
});
export default theme;
