import * as React from "react";
import * as classnames from "classnames";
import { connect } from "react-redux";
import { setOutput } from "../../actions";
import { UserException, getChemicalMolarMass, ElementData } from "../../api";
import { bindActionCreators, Dispatch } from "redux";
import { withStyles, createStyles, Theme } from "@material-ui/core/styles";
import { PeriodicTable } from "../ui/PeriodicTable";
import { AppData } from "../../types";
let data = require("../../data.json");

const styles = (theme: Theme) => createStyles({
  input: {
    width: "100%",
  },
});
export interface MainProps {
  output: string;
  setOutput: (output: string) => void;
  classes?: {[key: string]: string};
}

export class MainComponent extends React.Component<MainProps> {
  private _inputElement: HTMLInputElement | null = null;
  public render(): React.ReactNode {
    const { output, classes } = this.props;
    return (
      <div>
        <input type="input" ref={this._inputRef} onChange={this._changeHandler} className={this.props.classes.input} />
         <div>{ output }</div>
         <PeriodicTable onElementPressed={this._handleElementClick} />
      </div>
    );
  }

  private _inputRef = (el: HTMLInputElement | null) => {
    console.log(el);
    this._inputElement = el;
  }

  private _changeHandler = (event: any) => {
      this._updateOutput();
  }
  private _handleElementClick(element: ElementData) {
    console.log(this._inputElement);
    if (!this._inputElement) return;
    const v = this._inputElement.value;

    this._inputElement.value = v.substr(0, this._inputElement.selectionStart) + element.symbol + v.substr(this._inputElement.selectionEnd);
    this._updateOutput();
    this._inputElement.focus();
  }
  private _updateOutput = () => {
    if (!this._inputElement) return;
    const value = this._inputElement.value;
    if (value === "") {
      this.props.setOutput("");
      return;
    }
    try {
      const mass = getChemicalMolarMass(value);
      console.log(mass);
      this.props.setOutput(Math.round(mass * 1000)/1000 + "");
    } catch (e) {
      if (e instanceof UserException) {
        this.props.setOutput(e.message);
      }
    }
  }
}

const mapStateToProps = (state: AppData) => ({
	output: state.output,
});

const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
  setOutput,
}, dispatch);

export const Main = connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(MainComponent));
