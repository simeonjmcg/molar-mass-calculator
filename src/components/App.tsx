import * as React from "react";
import { hot } from "react-hot-loader";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";
import { withStyles, createStyles, Theme } from "@material-ui/core/styles";
import { withRouter, Link, Switch, Route } from "react-router-dom";

import { openSideNav, closeSideNav } from "../actions";
import { AppData } from "../types";

import { Navigation } from "./Navigation";
import { TitleBar } from "./TitleBar";

import { Main } from "./pages/Main";

const styles = (theme: Theme) => createStyles({
  app: {
    display: "flex",
    flexDirection: "column",
    position: "absolute",
    height: "100%",
    width: "100%",
  },
  contentContainer: {
    zIndex: theme.zIndex.appBar,
    display: 'flex',
  },
  contentClass: {
    flexGrow: 1,
    overflow: "auto",
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
  },
});
export interface AppProps {
  sideNavOpened: boolean;
  openSideNav: () => any;
  closeSideNav: () => any;
  classes?: any;
}

export class AppComponent extends React.Component<AppProps> {
	public render(): JSX.Element {
    const { sideNavOpened, classes } = this.props;
		return (
			<div className={classes.app}>
        <TitleBar onMenuClick={this.handleToggleOpen} />
        <div className={classes.contentContainer}>
          <Navigation opened={sideNavOpened}/>
          <div className={classes.contentClass}>
            <Switch>
              <Route exact path="/" component={Main} />
            </Switch>
          </div>
        </div>
      </div>
    );
  }
  public handleToggleOpen = () => {
    this.props.sideNavOpened ? this.props.closeSideNav() : this.props.openSideNav();
  }
}

const mapStateToProps = (state: AppData) => ({
  sideNavOpened: state.sideNavOpened,
});
const mapDispatchToProps = (dispatch: Dispatch) => bindActionCreators({
  openSideNav,
  closeSideNav,
}, dispatch);

export const App = hot(module)(connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(withRouter(AppComponent as any))));
