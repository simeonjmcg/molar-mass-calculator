import * as React from "react";

import { withStyles, createStyles, Theme } from "@material-ui/core/styles";
import { AppBar, Toolbar, Typography, Button, IconButton } from "@material-ui/core";
import { Menu as MenuIcon } from "@material-ui/icons";

const styles = (theme: Theme) => createStyles({
  titleBar: {
    zIndex: theme.zIndex.appBar + 1,
    flexGrow: 0,
  },
  flex: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
});

export interface TitleBarProps extends React.AllHTMLAttributes<HTMLDivElement> {
  onMenuClick?: (event: React.MouseEvent) => void;
  classes?: any;
}


const styleDecorator = withStyles(styles) as any;
@styleDecorator
export class TitleBar extends React.Component<TitleBarProps> {
  public render(): React.ReactNode {
    const {onClick, onMenuClick, classes, ...props} = this.props;
    return (
      <div className={classes.titleBar}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              className={classes.menuButton}
               color="inherit" aria-label="Menu"
                onClick={onMenuClick}>
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" className={classes.flex}>
              Molar Mass Calculator
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}
