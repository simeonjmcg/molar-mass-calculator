import * as React from "react";
import * as classnames from "classnames";
import { bindActionCreators, Dispatch } from "redux";
import { withStyles, createStyles, Theme } from "@material-ui/core/styles";
import { ElementData } from "../../api";
import { AppData } from "../../types";
let data = require("../../data.json");

const styles = createStyles({
  element: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    height: "100%",
    textAlign: "center",
    border: "solid gray 1px",
    borderRadius: "2px",
    fontWeight: "bold",
  },
  number: {
    fontSize: "10px",
  },
  symbol: {

  },
  mass: {
    fontSize: "10px",
  },
  other: {
    backgroundColor: "#9ab29a",
  },
  diatomicNonmetal: {
    backgroundColor: "#0591c6",
  },
  polyatomicNonmetal: {
    backgroundColor: "#00af89",
  },
  nobleGas: {
    backgroundColor: "#af44c9",
  },
  alkaliMetal: {
    backgroundColor: "#fa0516",
  },
  alkalineEarthMetal: {
    backgroundColor: "#ff610a",
  },
  metalloid: {
    backgroundColor: "#aed626",
  },
  transitionMetal: {
    backgroundColor: "#fab710",
  },
  postTransitionMetal: {
    backgroundColor: "#fdda02",
  },
  lanthanide: {
    backgroundColor: "#d9aa7c",
  },
  actinide: {
    backgroundColor: "#abb2cc",
  }
});

export interface PeriodicTableProps {
  onElementPressed?: (element: any) => void;
  classes?: {[key: string]: string};
}

export class PeriodicTableComponent extends React.Component<PeriodicTableProps> {
  private _elementPositionMap: {[key: string]: ElementData | null} = {};
  public render(): React.ReactNode {
    const { classes } = this.props;
    const rows = [];
    for (let i = 1; i <= 10; i++) {
      const cols = [];
      for (let j = 1; j <= 18; j++) {
        const key = i + "," + j;
        if (this._elementPositionMap[key] === undefined) {
          let found = false;
          for (let k = 0; k < data.elements.length; k++) {
            const e = data.elements[k];
            if (e && e.xpos === j && e.ypos === i) { 
              found = true;
              this._elementPositionMap[key] = e;
              break;
            }
          }
          if (!found)
            this._elementPositionMap[key] = null;
        }
        const el = this._elementPositionMap[key];
        if (el !== null) {
          cols.push(
          <td key={key}>
            <div
              className={classnames(classes.element, this._classnameFromCategory(el.category))}
              onPointerUp={() => this._handleElementClick(el)}>
              { el.number !== undefined &&
                <div className={classes.number}>{el.number}</div> }
              <div className={classes.symbol}>{el.symbol}</div>
              { el.atomic_mass !== undefined && 
                <div className={classes.mass}>{Math.round(el.atomic_mass*1000)/1000}</div> }
            </div>
           </td>);
        } else {
          cols.push(<td key={key}></td>);
        }
      }
      rows.push(<tr key={i}>{cols}</tr>);
    }
    return (
     <table>
       <tbody>
        {rows}
       </tbody>
     </table>
    );
  }

  private _handleElementClick(element: ElementData) {
    if (this.props.onElementPressed)
      this.props.onElementPressed(element);
  }

  private _classnameFromCategory = (category: string) => {
    const classes = this.props.classes;
    switch (category) {
      case "diatomic nonmetal":
        return classes.diatomicNonmetal;
      case "polyatomic nonmetal":
        return classes.polyatomicNonmetal;
      case "noble gas":
        return classes.nobleGas;
      case "alkali metal":
        return classes.alkaliMetal;
      case "alkaline earth metal":
        return classes.alkalineEarthMetal;
      case "metalloid":
        return classes.metalloid;
      case "transition metal":
        return classes.transitionMetal;
      case "post-transition metal":
        return classes.postTransitionMetal;
      case "lanthanide":
        return classes.lanthanide;
      case "actinide":
        return classes.actinide;
    }
    return classes.other;
  }
}

export const PeriodicTable = withStyles(styles)(PeriodicTableComponent);
