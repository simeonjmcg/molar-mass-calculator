import * as React from "react";
import classNames from 'classnames';
import { withStyles, createStyles, Theme } from '@material-ui/core/styles';
import { Dashboard as DashboardIcon, ViewList as ViewListIcon, CalendarToday as CalendarTodayIcon } from "@material-ui/icons";

import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";

import { Drawer, List, ListItem, ListItemIcon, ListItemText, Tooltip} from '@material-ui/core';
import { Link } from "react-router-dom";

export interface NavigationProps {
  opened: boolean;
  classes?: any;
}

const drawerWidth = 240;

const styles = (theme: Theme) => createStyles({
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
});

export class NavigationComponent extends React.Component<NavigationProps> {
  public render(): React.ReactNode {
    const {opened, classes} = this.props;
    return (
      <Drawer
        variant="permanent"
        classes={{
          paper: classNames(classes.drawerPaper, !opened && classes.drawerPaperClose),
        }}
        open={opened}
        >
         <List>
          {[
            ["/", "Dashboard", DashboardIcon],
          ].map((d, index) => {
            const Icon = d[2];
            const listitem = (
              <ListItem key={index} component={(props: any) => <Link to={d[0]} {...props} />} button>
                <ListItemIcon
                  className={classes.menuButton}>
                    <Icon />
                </ListItemIcon>
                <ListItemText primary={d[1]} />
              </ListItem>
            );
            if (opened)
              return listitem;
            else
              return (
                <Tooltip key={index} title={d[1]} placement="right">{listitem}</Tooltip>
              );
          })}
        </List>
      </Drawer>
    );
  }
}
export const Navigation = withStyles(styles)(NavigationComponent);
