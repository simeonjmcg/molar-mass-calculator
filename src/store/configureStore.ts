import { createStore, applyMiddleware, DeepPartial } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';

let mod = module as any;

const configureStore = (preloadedState: any) => {
  const store = createStore(
    rootReducer,
    preloadedState,
    applyMiddleware(thunk)
  );

  if (mod.hot) {
    // Enable Webpack hot module replacement for reducers
    mod.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers').default;
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
};

export default configureStore;