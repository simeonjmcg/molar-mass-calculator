
export interface ElementData {
  name: string;
  appearance: string;
  atomic_mass: number;
  boil: number;
  category: string;
  color: null;
  density: number;
  discovered_by: string;
  melt: number;
  molar_heat: number;
  named_by: string;
  ["number"]: number;
  period: number;
  phase: string;
  source: string;
  spectral_img: string;
  summary: string;
  symbol: string;
  xpos: number;
  ypos: number;
  shells: number[];
}

const data = require("../data.json") as {elements: ElementData[]};

export class UserException extends Error {
  constructor (message: string) {
    super(message);
    Object.setPrototypeOf(this, new.target.prototype); // restore prototype chain
  }
}

export enum TokenType {
  NAME = "name",
  NUMBER = "number",
  OPAREN = "open paren",
  CPAREN = "close paren",
}

export class Token {
  constructor (public type: TokenType, public value: string | number) {}
}

export class Compound {
  constructor (public chemicals: Chemical[], public coefficient: number) {};
}
export class Chemical {
  constructor (public value: string | Compound, public num: number) {};
}

export class Tokenizer {
  public static tokenize = (str: string): Token[] | undefined => {
    if(!str) {
      return undefined;
    }
    let tokens: Token[] = [];
    let type: TokenType;
    let val: string | number | undefined;
    let l = str.length;
    for(let i=0; i<l; i++) {
      let t: TokenType | undefined; // current type of character
      let ch = str.charCodeAt(i); // character integer code
      
      if((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122)) {                    
        // 'A' <= ch <= 'Z' || 'a' <= ch <= 'z'
        t = TokenType.NAME;
      } else if(ch >= 48 && ch <= 57) { 
        // '0' <= ch <= '9'
        t = TokenType.NUMBER;
      } else if(ch === 40) { 
        // ch == '('
        t = TokenType.OPAREN;
      } else if(ch === 41) { 
        // ch == ')'
        t = TokenType.CPAREN;
      } else if (ch !== 32) {
        throw new UserException("Character not recognized: '" + str.charAt(i) + "'");
      }
      
      // if type changed, terminate current token and start a new token
      if(t !== type && type !== undefined) {
        tokens.push(new Token(type, val));
        val = undefined;
      }
      // set values based of of type detected
      switch(t) {
        case TokenType.NAME:
          if(val === undefined) {
            val = "";
          }
          if(t === type && ch >= 65 && ch <= 90) { 
            // if continuing chars are upper case, start new token
            tokens.push(new Token(type, val));
            val = "";
          }
          val += str.charAt(i);
          break;
        case TokenType.OPAREN:
        case TokenType.CPAREN:
          // Start new token for each parenthesis
          if(t == type) {
            tokens.push(new Token(type, val));
          }
          val = str.charAt(i);
          break;
        case TokenType.NUMBER:
          if(val === undefined) {
            val = 0;
          }
          // char code to integer conversion
          val = val as number * 10 + (ch - 48);
          break;
      }
      // set type to t for next round
      type = t;
    }
    // finalize last token
    if(type !== undefined) {
      tokens.push(new Token(type, val));
    }
    return tokens;
  }; 
}

export class Parser {
  public static parse = (tokens: Token[], level: number = 0) =>  {
    if(tokens === undefined) {
      throw new UserException("No input found.");
    }
    
    const compound = new Compound([], 1);
    const l = tokens.length;
    for(let i = 0; i < l; i++) {
    
      if(i === 0 && tokens[i].type === TokenType.NUMBER) { 
          // if '$[num]'
        
        compound.coefficient = tokens[i].value as number;
        
      } else if(tokens[i].type === TokenType.NAME && 
            tokens[i+1] !== undefined && tokens[i+1].type === TokenType.NUMBER) {
            // if '[name][num]'
        
        compound.chemicals.push(
            new Chemical(tokens[i].value as string, tokens[i+1].value as number));
        
        i++; // skip double token(name and num)
        
      } else if(tokens[i].type === TokenType.NAME) {
          // if '[name]'
        
        compound.chemicals.push(
            new Chemical(tokens[i].value as string, 1));
        
      } else if(tokens[i].type === TokenType.OPAREN) {
          // if '('
        
        // recursively parse values within parenthesis
        let parseObject = Parser.parse(tokens.slice(i + 1), level + 1);
        
        compound.chemicals.push(
            new Chemical(parseObject.compound, parseObject.num));
        
        // skip is undefined if no closing paren is found
        if(parseObject.skip === undefined) {
          throw new UserException("Missing closing parenthesis");
        }
        
        i += parseObject.skip;
      } else if(tokens[i].type === TokenType.CPAREN) {
          // if ')'
          
        // level is undefined if funcition hasn't recursed
        if(level === 0) {
          throw new UserException("Missing opening parenthisis");
        }
        if(tokens[i+1] !== undefined && tokens[i+1].type === TokenType.NUMBER) { 
          return {compound, num: tokens[i + 1].value as number, skip: i + 2};
        }
        return { compound, num: 1, skip: i + 1 };
        
      } else {
        throw new UserException("Syntax error");
      }
    }
    return {compound, num: 1, skip: undefined};
  }
}

const elementNameMap: {[key: string]: ElementData} = {};
for(let j = 0; j < data.elements.length; j++) {
  const symbol = data.elements[j].symbol.toLowerCase();
  elementNameMap[symbol] = data.elements[j];
}

const evaluate = (compound: Compound) => {
  if(compound === undefined || compound.chemicals === undefined || compound.chemicals.length === 0) {
    throw new Error("Invalid compound input");
  }
  
  let total = 0,
    equation = "",
      l = compound.chemicals.length;
  
  // loop through chemicals
  for(let i=0; i<l; i++) {
    let c = compound.chemicals[i];
    // add ' + ' between amounts
    if(i > 0) {
      equation += " + ";
    }
    // if chem is a symbol
    if(typeof c.value === "string") {
      // symbol not case sensitive
      let symbol = c.value.toLowerCase();
      const element = elementNameMap[symbol];
      if(element === undefined) {
        throw new UserException("Chemical symbol not recognized: " + symbol);
      }
      
      total += element.atomic_mass * c.num;
      
      equation += `${element.atomic_mass}`;
      if(c.num !== 1) {
        equation += ` * ${c.num}`;
      }
    } else if(typeof c.value === "object") {
      // recursively calculate subcompound.
      var subcompound = evaluate(c.value);
      total += subcompound.total * c.num;
      
      equation = `${equation}(${subcompound.equation})`;
      if(c.num !== 1) {
        equation = `${equation} * ${c.num}`;
      }
    } else {
      throw new Error("Invalid chemical input");
    }
  }
  if(compound.coefficient !== 1) {
    total *= compound.coefficient;
    equation = `${compound.coefficient} * (${equation})`;
  }
  if(!equation) {
    equation = "0";
  }
  return {total, equation};
};

export const getChemicalMolarMass = (str: string) => {
  if(str === undefined) {
    return 0;
  }
  let tokens = Tokenizer.tokenize(str);
  let r = Parser.parse(tokens);
  
  if(r === undefined) {
    throw new Error("no chemicals parsed");
  }
  const e = evaluate(r.compound);
  return e.total;
};
