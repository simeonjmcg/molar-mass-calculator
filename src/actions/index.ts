import actionCreatorFactory from "typescript-fsa";

const actionCreator = actionCreatorFactory();

export const SET_INPUT = actionCreator<string>("SET_INPUT");
export const SET_OUTPUT = actionCreator<string>("SET_OUTPUT");
export const OPEN_SIDE_NAV = actionCreator("OPEN_SIDE_NAV");
export const CLOSE_SIDE_NAV = actionCreator("CLOSE_SIDE_NAV");

export const setInput = (input: string) => {
  return (dispatch: any) => dispatch(SET_INPUT(input));
}

export const setOutput = (output: string) => {
  return (dispatch: any) => dispatch(SET_OUTPUT(output));
}

export const openSideNav = () => (dispatch: any) => dispatch(OPEN_SIDE_NAV());
export const closeSideNav = () => (dispatch: any) => dispatch(CLOSE_SIDE_NAV());
