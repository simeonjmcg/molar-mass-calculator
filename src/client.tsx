import * as React from "react";
import { hydrate } from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import JssProvider from "react-jss/lib/JssProvider";
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName,
} from "@material-ui/core/styles";
import configureStore from "./store/configureStore";

import { theme } from "./theme";
import { AppData } from "./types";

import { App } from "./components/App";

const preloadedState = (window as any).__PRELOADED_STATE__ as AppData;
const store = configureStore(preloadedState);
const rootElement = document.getElementById("root");

class Root extends React.Component {
  // Remove the server-side injected CSS.
  public componentDidMount() {
    const jssStyles = document.getElementById('jss-server-side');
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  public render(): React.ReactNode {
    return <App />
  }
}

// Create a new class name generator.
const generateClassName = createGenerateClassName();

hydrate(
  <JssProvider generateClassName={generateClassName}>
    <MuiThemeProvider theme={theme}>
      <Provider store={store}>
        <BrowserRouter> 
          <Root />
        </BrowserRouter>
      </Provider>
    </MuiThemeProvider>
  </JssProvider>,
  rootElement,
);
