# README #
Calculator that takes user inputted chemical formulas and calculates the Molar Mass of the given molecule.
Input is parsed, lexed, and then evaluated and displayed.
This project  is written in React.js, Typescript, and Redux.

### Getting Started ###

Before you start, you will need Node.js and npm.

* First step is just to clone this repository: `git clone https://simeonjmcg@bitbucket.org/simeonjmcg/molar-mass-calculator.git`
* run `npm install`
* finally, `npm start` should run the project
